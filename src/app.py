from flask import Flask
import joblib
from service.models import download_model
from service.data import eval_metrics, get_test_data
import os

app = Flask(__name__)


@app.route("/", methods=["GET"])
def main():
    return "IP Camp Service API"


@app.route("/test", methods=["GET"])
def test():
    download_model()
    test_x, test_y = get_test_data()
    lr_from_joblib = joblib.load("test-model.pkl")
    predicted_qualities = lr_from_joblib.predict(test_x)
    (rmse, mae, r2) = eval_metrics(test_y, predicted_qualities)
    return "%s, %s, %s" % (rmse, mae, r2)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 5001)))
